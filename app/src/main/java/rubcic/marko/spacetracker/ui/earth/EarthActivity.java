package rubcic.marko.spacetracker.ui.earth;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import rubcic.marko.spacetracker.R;

public class EarthActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earth);
    }
}
