package rubcic.marko.spacetracker.ui.mars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import rubcic.marko.spacetracker.R;
import rubcic.marko.spacetracker.ui.mars.rover.RoverActivity;

public class MarsActivity extends AppCompatActivity implements MarsMvp.View {

    private CardView cvCuriosity;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mars);
        initViews();
    }

    private void initViews() {
        toolbar = findViewById(R.id.tbMars);
        cvCuriosity = findViewById(R.id.cvCuriosity);
        setListeners();
    }

    private void setListeners() {
        cvCuriosity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRoverActivity("Curiosity");
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    @Override
    public void openRoverActivity(String roverName) {
        Intent intent = new Intent(this, RoverActivity.class);
        intent.putExtra(Intent.EXTRA_SUBJECT,roverName);
        startActivity(intent);
    }
}
