package rubcic.marko.spacetracker.ui.mars.rover;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rubcic.marko.spacetracker.data.models.PhotoManifest;
import rubcic.marko.spacetracker.data.models.Rover;
import rubcic.marko.spacetracker.data.networking.retrofit.ApiService;
import rubcic.marko.spacetracker.data.networking.retrofit.RetrofitClient;

public class RoverPresenterImpl implements RoverMVP.Presenter {

    private ApiService apiService;
    private RoverMVP.View view;

    RoverPresenterImpl(RoverMVP.View view){
        apiService = RetrofitClient.getApiService();
        this.view = view;

    }

    //TODO authentifikacija
    @Override
    public void getRoverData(String roverName) {
        apiService.getPhotoManifest(roverName,"1jV5YoEHoTrVTOJ3EB8tXmDe0wg9IxEK4chOuUhF").enqueue(new Callback<PhotoManifest>() {
            @Override
            public void onResponse(Call<PhotoManifest> call, Response<PhotoManifest> response) {
                if(response.isSuccessful())
                    view.showRoverInfo(response.body().getRover());
                else
                    view.showToast("Something went wrong!");
            }

            @Override
            public void onFailure(Call<PhotoManifest> call, Throwable t) {
                    view.showToast("Something went wrong!");
            }
        });

    }
}
