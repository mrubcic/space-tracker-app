package rubcic.marko.spacetracker.ui.mars;

import rubcic.marko.spacetracker.data.networking.retrofit.ApiService;
import rubcic.marko.spacetracker.data.networking.retrofit.RetrofitClient;

public class MarsPresenterImpl implements MarsMvp.Presenter{

    private MarsMvp.View view;
    private ApiService apiService;

    public MarsPresenterImpl( MarsMvp.View view){
        this.view = view;
        init();
    }

    private void init() {
        apiService = RetrofitClient.getApiService();
    }
}
