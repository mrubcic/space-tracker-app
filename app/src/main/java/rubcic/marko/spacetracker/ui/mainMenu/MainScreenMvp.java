package rubcic.marko.spacetracker.ui.mainMenu;

public interface MainScreenMvp {

    interface View{

        void initViews();

        void openEarthActivity();

        void openMarsActivity();



    }

    interface Presenter{


    }
}
