package rubcic.marko.spacetracker.ui.mainMenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;

import rubcic.marko.spacetracker.R;
import rubcic.marko.spacetracker.ui.earth.EarthActivity;
import rubcic.marko.spacetracker.ui.mars.MarsActivity;

public class MainScreenActivity extends AppCompatActivity implements MainScreenMvp.View {

   private LinearLayout llEarth;
   private LinearLayout llMars;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    public void initViews() {
        llEarth = findViewById(R.id.llEarth);
        llMars  = findViewById(R.id.llMars);

        setClickListeners();
    }

    private void setClickListeners() {
        llEarth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEarthActivity();
            }
        });

        llMars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMarsActivity();
            }
        });
    }

    @Override
    public void openEarthActivity() {
        Intent intent = new Intent(this, EarthActivity.class);
        startActivity(intent);

    }

    @Override
    public void openMarsActivity() {
        Intent intent = new Intent(this, MarsActivity.class);
        startActivity(intent);
    }

}
