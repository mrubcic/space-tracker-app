package rubcic.marko.spacetracker.ui.mars;

import rubcic.marko.spacetracker.data.networking.retrofit.ApiService;

public interface MarsMvp {


    interface View{
        /**
         * Open desired rover activity
         * @param roverName rover name
         */
        void openRoverActivity(String roverName);

    }

    interface Presenter{



    }
}
