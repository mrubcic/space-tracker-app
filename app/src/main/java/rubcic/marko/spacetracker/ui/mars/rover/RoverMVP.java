package rubcic.marko.spacetracker.ui.mars.rover;

import rubcic.marko.spacetracker.data.models.Rover;

public interface RoverMVP {

    interface View{

        void showRoverInfo(Rover rover);

        void showToast(String message);

    }

    interface Presenter{

        void getRoverData(String roverName);

    }
}
