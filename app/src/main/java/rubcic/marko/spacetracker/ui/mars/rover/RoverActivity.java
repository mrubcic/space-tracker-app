package rubcic.marko.spacetracker.ui.mars.rover;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import rubcic.marko.spacetracker.R;
import rubcic.marko.spacetracker.data.models.Rover;

public class RoverActivity extends AppCompatActivity implements RoverMVP.View {

    private Toolbar toolbar;
    private RoverPresenterImpl roverPresenter = new RoverPresenterImpl(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rover);
        initViews();

        roverPresenter.getRoverData(getIntent().getStringExtra(Intent.EXTRA_SUBJECT));
    }

    private void initViews() {
        toolbar = findViewById(R.id.tbRover);
        setClickListeners();
    }

    private void setClickListeners() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void showRoverInfo(Rover rover) {
        toolbar.setTitle(rover.getName());
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show();
    }


}
