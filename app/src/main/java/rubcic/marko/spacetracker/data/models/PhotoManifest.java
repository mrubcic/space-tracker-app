package rubcic.marko.spacetracker.data.models;

import com.squareup.moshi.Json;

public class PhotoManifest {

    @Json(name = "photo_manifest")
    private Rover rover;

    public PhotoManifest(Rover rover) {
        this.rover = rover;
    }

    public Rover getRover() {
        return rover;
    }
}
