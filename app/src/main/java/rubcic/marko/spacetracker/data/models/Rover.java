package rubcic.marko.spacetracker.data.models;

import com.squareup.moshi.Json;

import java.util.List;


public class Rover {

    @Json(name = "name")
    private String name;
    @Json(name = "landing_date")
    private String landingDate;
    @Json(name = "launch_date")
    private String launchDate;
    @Json(name = "status")
    private String status;
    @Json(name = "max_sol")
    private Integer maxSol;
    @Json(name = "max_date")
    private String maxPhotoDate;
    @Json(name = "total_photos")
    private Integer sumPhotos;
    @Json(name = "photos")
    private List<Photo> photos;

    public Rover(String name, String landingDate, String launchDate, String status, Integer maxSol, String maxPhotoDate, Integer sumPhotos, List<Photo> photos) {
        this.name = name;
        this.landingDate = landingDate;
        this.launchDate = launchDate;
        this.status = status;
        this.maxSol = maxSol;
        this.maxPhotoDate = maxPhotoDate;
        this.sumPhotos = sumPhotos;
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public String getLandingDate() {
        return landingDate;
    }

    public String getLaunchDate() {
        return launchDate;
    }

    public String getStatus() {
        return status;
    }

    public Integer getMaxSol() {
        return maxSol;
    }

    public String getMaxPhotoDate() {
        return maxPhotoDate;
    }

    public Integer getSumPhotos() {
        return sumPhotos;
    }

    public List<Photo> getPhotos() {
        return photos;
    }
}
