package rubcic.marko.spacetracker.data.models;

import com.squareup.moshi.Json;

import java.util.List;

class Photo {

    @Json(name = "sol" )
    private Integer sol;
    @Json(name = "earth_date")
    private String earth_date;
    @Json(name = "total_photos")
    private Integer totalPhotos;
    @Json(name = "cameras")
    private List<String> cameras;

    public Photo(Integer sol, String earth_date, Integer totalPhotos, List<String> cameras) {
        this.sol = sol;
        this.earth_date = earth_date;
        this.totalPhotos = totalPhotos;
        this.cameras = cameras;
    }

    public Integer getSol() {
        return sol;
    }

    public String getEarth_date() {
        return earth_date;
    }

    public Integer getTotalPhotos() {
        return totalPhotos;
    }

    public List<String> getCameras() {
        return cameras;
    }
}
