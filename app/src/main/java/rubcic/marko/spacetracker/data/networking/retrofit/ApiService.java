package rubcic.marko.spacetracker.data.networking.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rubcic.marko.spacetracker.data.models.PhotoManifest;

public interface ApiService {

    String apiKey = "1jV5YoEHoTrVTOJ3EB8tXmDe0wg9IxEK4chOuUhF";

    @GET("manifests/{roverName}")
    Call<PhotoManifest> getPhotoManifest(@Path("roverName") String roverName, @Query("api_key") String apiKey);
}
