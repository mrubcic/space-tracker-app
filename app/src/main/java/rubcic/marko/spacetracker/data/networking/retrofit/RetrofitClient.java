package rubcic.marko.spacetracker.data.networking.retrofit;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class RetrofitClient {

    private static final String BASE_URL = "https://api.nasa.gov/mars-photos/api/v1/";
    private static ApiService apiService;
    private static final String apiKey = "1jV5YoEHoTrVTOJ3EB8tXmDe0wg9IxEK4chOuUhF";

    /**
     * Create Retrofit client if not created, else return existing instance.
     * @return Retrofit client
     */
    public static ApiService getApiService(){
        if(apiService == null){
            apiService = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .client(getOkHttpClient())
                    .build()
                    .create(ApiService.class);
        }

        return apiService;
    }

    private static OkHttpClient getOkHttpClient(){
       OkHttpClient client =  new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("api_key",apiKey)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();

       return client;

    }
}
